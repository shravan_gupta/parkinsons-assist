/*
 SetActivityGoal.swift
 Worked on by: Matthew Marinets, Malcolm Mckean, Vi Thai
 Changes:
 -Initial Creation
 -UI Changes
 -Displays current activity goal
 -Firebase implementation to fetch and update goal data on database.
 -Added long press button functionality
 Bugs: N/A
 Description: View controller for the activity goal setting screen
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import UIKit

/*
 View Controller for the activity goal setting screen
 */
class SetActivityGoal: UIViewController {
    @IBOutlet weak var setGoal: UIButton!
    @IBOutlet weak var one: UIButton!
    @IBOutlet weak var two: UIButton!
    @IBOutlet weak var three: UIButton!
    @IBOutlet weak var four: UIButton!
    @IBOutlet weak var five: UIButton!
    @IBOutlet weak var six: UIButton!
    @IBOutlet weak var seven: UIButton!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var currentGoalLabel: UILabel!
    @IBOutlet weak var backgroundTop: UIImageView!
    
    var numActiveDays = ""
    var db : Firestore!
    let docRef = CurrentUser.FirebaseDocRef!
    
    override func viewDidLoad() {
        setGoal.layer.cornerRadius = setGoal.frame.height * 0.5
        one.layer.cornerRadius = one.frame.height * 0.5
        two.layer.cornerRadius = two.frame.height * 0.5
        three.layer.cornerRadius = three.frame.height * 0.5
        four.layer.cornerRadius = four.frame.height * 0.5
        five.layer.cornerRadius = five.frame.height * 0.5
        six.layer.cornerRadius = six.frame.height * 0.5
        seven.layer.cornerRadius = seven.frame.height * 0.5
        back.layer.cornerRadius = back.frame.height * 0.5
        backgroundTop.layer.cornerRadius = backgroundTop.frame.height * 0.35
        setActivityGoal()
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        fetchAndDisplayGoal()
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(tap1))
        let longGesture1 = UILongPressGestureRecognizer(target: self, action: #selector(long1))
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(tap2))
        let longGesture2 = UILongPressGestureRecognizer(target: self, action: #selector(long2))
        let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(tap3))
        let longGesture3 = UILongPressGestureRecognizer(target: self, action: #selector(long3))
        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(tap4))
        let longGesture4 = UILongPressGestureRecognizer(target: self, action: #selector(long4))
        let tapGesture5 = UITapGestureRecognizer(target: self, action: #selector(tap5))
        let longGesture5 = UILongPressGestureRecognizer(target: self, action: #selector(long5))
        let tapGesture6 = UITapGestureRecognizer(target: self, action: #selector(tap6))
        let longGesture6 = UILongPressGestureRecognizer(target: self, action: #selector(long6))
        let tapGesture7 = UITapGestureRecognizer(target: self, action: #selector(tap7))
        let longGesture7 = UILongPressGestureRecognizer(target: self, action: #selector(long7))
        let tapGestureBack = UITapGestureRecognizer(target: self, action: #selector(tapBack))
        let longGestureBack = UILongPressGestureRecognizer(target: self, action: #selector(longBack))
        let tapGestureSetGoal = UITapGestureRecognizer(target: self, action: #selector(tapSetGoal))
        let longGestureSetGoal = UILongPressGestureRecognizer(target: self, action: #selector(longSetGoal))
        
        
        one.addGestureRecognizer(tapGesture1)
        one.addGestureRecognizer(longGesture1)
        two.addGestureRecognizer(tapGesture2)
        two.addGestureRecognizer(longGesture2)
        three.addGestureRecognizer(tapGesture3)
        three.addGestureRecognizer(longGesture3)
        four.addGestureRecognizer(tapGesture4)
        four.addGestureRecognizer(longGesture4)
        five.addGestureRecognizer(tapGesture5)
        five.addGestureRecognizer(longGesture5)
        six.addGestureRecognizer(tapGesture6)
        six.addGestureRecognizer(longGesture6)
        seven.addGestureRecognizer(tapGesture7)
        seven.addGestureRecognizer(longGesture7)
        back.addGestureRecognizer(tapGestureBack)
        back.addGestureRecognizer(longGestureBack)
        setGoal.addGestureRecognizer(tapGestureSetGoal)
        setGoal.addGestureRecognizer(longGestureSetGoal)
    }
    
    func fetchAndDisplayGoal() {
        docRef.getDocument() { (document, error) in
            if let dbGoal = document!.data()!["goal"], "\(dbGoal)" != "" {
                self.currentGoalLabel.text = "Current Goal: \(dbGoal) days of activity"
                self.numActiveDays = "\(dbGoal)"
            } else {
                self.currentGoalLabel.text = "Activity Goal"
                self.numActiveDays = ""
            }
        }
    }
    
    @objc func tap1(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            setNumActiveDays(sender: one)
        }
    }
    
    @objc func long1(){
        if(CurrentUser.LongPress)
        {
            setNumActiveDays(sender: one)
        }
        else
        {
            
        }
    }
    
    @objc func tap2(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            setNumActiveDays(sender: two)
        }
    }
    
    @objc func long2(){
        if(CurrentUser.LongPress)
        {
            setNumActiveDays(sender: two)
        }
        else
        {
            
        }
    }
    
    @objc func tap3(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            setNumActiveDays(sender: three)
        }
    }
    
    @objc func long3(){
        if(CurrentUser.LongPress)
        {
            setNumActiveDays(sender: three)
        }
        else
        {
            
        }
    }
    
    @objc func tap4(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            setNumActiveDays(sender: four)
        }
    }
    
    @objc func long4(){
        if(CurrentUser.LongPress)
        {
            setNumActiveDays(sender: four)
        }
        else
        {
            
        }
    }
    
    @objc func tap5(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            setNumActiveDays(sender: five)
        }
    }
    
    @objc func long5(){
        if(CurrentUser.LongPress)
        {
            setNumActiveDays(sender: five)
        }
        else
        {
            
        }
    }
    
    @objc func tap6(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            setNumActiveDays(sender: six)
        }
    }
    
    @objc func long6(){
        if(CurrentUser.LongPress)
        {
            setNumActiveDays(sender: six)
        }
        else
        {
            
        }
    }
    
    @objc func tap7(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            setNumActiveDays(sender: seven)
        }
    }
    
    @objc func long7(){
        if(CurrentUser.LongPress)
        {
            setNumActiveDays(sender: seven)
        }
        else
        {
            
        }
    }
    
    @objc func tapBack(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            performSegue(withIdentifier: "backToHealthScreen", sender: self)
        }
    }
    
    @objc func longBack(){
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "backToHealthScreen", sender: self)
        }
        else
        {
            
        }
    }
    
    @objc func tapSetGoal(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            setActivityGoal()
        }
    }
    
    @objc func longSetGoal(){
        if(CurrentUser.LongPress)
        {
            setActivityGoal()
        }
        else
        {
            
        }
    }
    
    /*
     used to select desired number of active days per week
     */
    func setNumActiveDays(sender: UIButton) {
        numActiveDays = sender.titleLabel!.text!
        currentGoalLabel.text = "Current Goal: " + numActiveDays + " days of activity"
    }
    
    /*
     used to set number of active days per week to selected
     */
    func setActivityGoal() {
        if self.numActiveDays != "" {
            docRef.updateData(["goal" : numActiveDays])
        }
        performSegue(withIdentifier: "backToHealthScreen", sender: self)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
