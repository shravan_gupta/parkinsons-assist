/*
 HelperUI.swift
 Worked on by: Matthew Marinets, Vi Thai, Malcolm Mckean
 Changes:
 -Initial Creation
 -Added three Methods For UIButtons
 -Added set background color helper method for UIButton
 Bugs: N/A
 Description: Contains helpful methods that perform operations on UI objects.
 */

import Foundation
import UIKit

class HelperUI{
    //UIButton Methods
    static func enableUIButton(Button: UIButton){
        Button.isEnabled = true
        Button.alpha = 1
    }
    static func disableUIButton(Button: UIButton){
        Button.isEnabled = false
        Button.alpha = 0.5
    }
    static func disableAndMakeUIButtonIvisible(Button: UIButton){
        Button.isEnabled = false
        Button.alpha = 0
    }
    static func setUIButtonText(Button: UIButton, Text: String)
    {
        Button.setTitle(Text, for: .normal)
    }
    static func getUIButtonText(Button: UIButton)->String{
        return Button.titleLabel!.text!
    }
    static func setUIButtonColor(Button: UIButton, Color: UIColor?) {
        if Color != nil {
            Button.backgroundColor = Color!
        }
    }
}
