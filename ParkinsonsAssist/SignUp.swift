/*
 SignUp.swift
 Worked on by: Matthew Marinets, Vi Thai, Ramish Khan, Cooper Birks, Malcolm Mckean, Shravan Gupta
 Changes:
 -Initial Creation
 -UI Changes
 -Changed username and password to labels
 -Added name field
 Bugs: N/A
 Description: View Controller for the sign up screen
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import UIKit

import FirebaseCore
import FirebaseFirestore

/* View controller for the sign up screen */
class SignUp: UIViewController {
    /* For now collecting User data in Dict. Need to reimplement when we hook up firebase.*/
//    var UserDBLocal = UserDB()
    var db: Firestore!
//    var userExists = false
//    var passwordExists = false
    
    @IBOutlet weak var Username: UITextField!
    @IBOutlet weak var Password: UITextField!
    @IBOutlet weak var Name: UITextField!
    @IBOutlet weak var CreateButton: UIButton!
    @IBOutlet weak var GreySquare: UIImageView!
    @IBOutlet weak var LoginButton: UIButton!
    @IBOutlet weak var Error: UILabel!
    
    /* function called when the user clicks the create account button */
    @IBAction func pressCreate(_ sender: UIButton) {
        if Username.text != nil, Password.text != nil {
            let uid = Username.text!
            let pw = Password.text!
            let name = Name.text!
            
            if uid != "", pw != "" {
                db.collection("users").whereField("uid", isEqualTo: uid).getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    }
                    if querySnapshot?.count != 0 {
                        self.Error.text = "Username already exists."
//                        self.userExists = true
                    }
                    else{
                        
                        self.db.collection("users").addDocument(data: ["uid" : uid, "password" : pw, "name" : name, "longPress" : false])
                        self.performSegue(withIdentifier: "createSegue", sender: self)
                        
                        /* Vi commented out Malcolm's code. */
//                        self.db.collection("users").whereField("password", isEqualTo: pw).getDocuments() { (querySnapshot, err) in
//                            if let err = err{
//                                print("Error getting documents: \(err)")
//                            }
//                            if querySnapshot?.count != 0{
//                                self.Error.text = "Password already exists."
//                                self.passwordExists = true
//                            }
//                            else
//                            {
//                                self.db.collection("users").addDocument(data: ["uid" : uid, "password" : pw])
//                                self.performSegue(withIdentifier: "createSegue", sender: self)
//                            }
//                        }
                    }
                }
            }
            else{
                Error.text = "Please fill in the fields."
            }
        }
        else{
            Error.text = "Please fill in the fields."
        }
    }
    
    /*Called before we segue back to the sign in screen*/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let signInVC = segue.destination as? SignIn {
//        signInVC.userDB = self.UserDBLocal
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        db = Firestore.firestore()
    
        CreateButton.layer.cornerRadius = CreateButton.frame.height * 0.5
        GreySquare.layer.cornerRadius = GreySquare.frame.height * 0.10
        LoginButton.layer.cornerRadius = LoginButton.frame.height * 0.5
        

        LoginButton.layer.borderWidth = 2
        LoginButton.layer.borderColor = UIColor.lightGray.cgColor
    }
}
