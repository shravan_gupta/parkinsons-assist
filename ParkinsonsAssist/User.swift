/*
 User.swift
 Worked on by: Vi Thai
 Changes:
 -Initial Creation
 Bugs: N/A
 Description: Class used to model and store the user data locally.
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import Foundation

struct CurrentUser {
    static var Username = String()
    static var FirebaseDocRef : DocumentReference? = nil
    static var FirebaseDocId = String()
    static var LongPress = Bool()
}

/*User Class containing items like username and password*/
class User
{
    var Username: String
    var Password: String
    var LongPress: Bool = true
//    var UserKey: String
    
    init(username: String, password: String){
        self.Username = username;
        self.Password = password;
    }
    
    //Getters & Setters
    func getUsername() -> String{
        return self.Username
    }
    
    func getPassword() -> String{
        return self.Password
    }
    
    func getLongPress() -> Bool{
        return self.LongPress
    }
    
    func setUsername(username: String){
        self.Username = username
    }
    
    func setPassword(password: String){
        self.Password = password
    }
    
    func setLongPress(longpress: Bool){
        self.LongPress = longpress
    }
    
    //Password Verification
    func verifyPassword(input: String) -> Bool{
        return self.Password == input
    }
}
