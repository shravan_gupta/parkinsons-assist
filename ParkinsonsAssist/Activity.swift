/*
 Activity.swift
 Worked on by: Vi Thai
 Changes:
 -Initial Creation
 -Added Color and Icon decoders
 Bugs: N/A
 Description: Class used to model an activity set by user through Health Screen
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import Foundation
import UIKit

enum ActivityType : Int{
    case Social = 1
    case Physical = 0
}

class Activity {
    var ActivityName: String
    var TypeOfActivity: ActivityType
    var ActivityDate: Date!
    
    
    static let SocialActivityStrings = ["Cooking", "Movie Night", "Coffee", "Brunch", "Party"]
    static let PhysicalActivityStrings = ["Running", "Walking", "Bowling"]
    
    static let ActivityColorDecoder : [String : UIColor] = [
        "Cooking" :  #colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1),
        "Movie Night" :  #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1),
        "Coffee" :  #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1),
        "Brunch" :  #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1),
        "Party" :  #colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1),
        "Running" :  #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1),
        "Walking" :  #colorLiteral(red: 0.7080832568, green: 0.9686274529, blue: 0, alpha: 1),
        "Bowling" :  #colorLiteral(red: 0.9686274529, green: 0.7402292101, blue: 0.9118512969, alpha: 1)
    ]
    
    static let ActivityIconDecoder : [String : String] = [
        "Cooking" : "🍳",
        "Movie Night" : "🍿",
        "Coffee" : "☕️",
        "Brunch" : "🥞",
        "Party" : "🎉",
        "Running" : "🏃‍♂️",
        "Walking" : "🚶‍♂️",
        "Bowling" : "🎳"
    ]
    
    
    /*
     Gets the corresponding activity type from the string
     */
    static func getActivityType(activityName: String) -> ActivityType {
        if SocialActivityStrings.contains(activityName) {
            return ActivityType.Social
        }
        else {
            return ActivityType.Physical
        }
    }
    
    /*
     Constructor
     */
    init(activityString: String, activityDate: Date) {
        self.ActivityName = activityString
        self.ActivityDate = activityDate
        self.TypeOfActivity = Activity.getActivityType(activityName: activityString)
    }

    /*
     Constructor
     */
    init(activityString: String) {
        self.ActivityName = activityString
        self.TypeOfActivity = Activity.getActivityType(activityName: activityString)
    }
}
