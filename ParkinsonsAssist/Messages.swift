/*
 Messages.swift
 Worked on by: Cooper Birks, Malcolm Mckean, Ramish Khan
 Changes:
 -Initial Creation
 -Minor UI changes
 -Page Down working
 -Search working
 -All orderings working
 -Added long press button functionality
 -Page up working
 Bugs: N/A
 Description: Main file for the messaging screen
 Copyright © 2019 T3am SFU. All rights reserved.
 */
import UIKit

/*View controller for the messaging screen*/
class Messages: UIViewController {

    // Screens items
    @IBOutlet weak var SearchButton: UIButton!
    @IBOutlet weak var PageUpButton: UIButton!
    @IBOutlet weak var BackButton: UIButton!
    @IBOutlet weak var Background: UIImageView!
    @IBOutlet weak var BackgroundBottom: UIImageView!
    @IBOutlet weak var PageDown: UIButton!
    @IBOutlet weak var SortContacts: UIButton!
    @IBOutlet weak var Contact1: UIButton!
    @IBOutlet weak var Contact2: UIButton!
    @IBOutlet weak var Contact3: UIButton!
    @IBOutlet weak var OrderLabel: UILabel!
    
    var AlphaContactArray: [String] = []
    var TrustedContactArray: [String] = []
    var RecentContactArray: [String] = []
    
    var ContactName : String = ""
    var PageNum = 0
    var OrderAlphabetical = false
    var OrderRecent = true
    var OrderTrusted = false
    var contactArray : [UIButton] = []
    var SearchButtonEnabled : Bool = true
    var PageUpButtonEnabled : Bool = false
    var longGestureSortContacts : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longSortContacts))
    var longGestureC1 : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longC1))
    var longGestureC2 : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longC2))
    var longGestureC3 : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longC3))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SortContacts.setTitle("𝐀", for: .normal)
        //Screen overlay rounded corners
        SearchButton.layer.cornerRadius = SearchButton.frame.height * 0.5
        PageUpButton.layer.cornerRadius = PageUpButton.frame.height * 0.5
        BackButton.layer.cornerRadius = BackButton.frame.height * 0.5
        Background.layer.cornerRadius = Background.frame.height * 0.35
        BackgroundBottom.layer.cornerRadius = BackgroundBottom.frame.height * 0.35
        PageDown.layer.cornerRadius = PageDown.frame.height * 0.5
        SortContacts.layer.cornerRadius = SortContacts.frame.height * 0.5
        
        //Contact roudned corners
        Contact1.layer.cornerRadius = Contact1.frame.height * 0.5
        Contact2.layer.cornerRadius = Contact2.frame.height * 0.5
        Contact3.layer.cornerRadius = Contact3.frame.height * 0.5
        
        // border around contacts
        //Contact1.layer.borderWidth = 5
        //Contact1.layer.borderColor = UIColor.red.cgColor
        
        let tapGestureBack = UITapGestureRecognizer(target: self, action: #selector(tapBack))
        let longGestureBack = UILongPressGestureRecognizer(target: self, action: #selector(longBack))
        let tapGestureC1 = UITapGestureRecognizer(target: self, action: #selector(tapC1))
        longGestureC1 = UILongPressGestureRecognizer(target: self, action: #selector(longC1))
        let tapGestureC2 = UITapGestureRecognizer(target: self, action: #selector(tapC2))
        longGestureC2 = UILongPressGestureRecognizer(target: self, action: #selector(longC2))
        let tapGestureC3 = UITapGestureRecognizer(target: self, action: #selector(tapC3))
        longGestureC3 = UILongPressGestureRecognizer(target: self, action: #selector(longC3))
        let tapGesturePageUP = UITapGestureRecognizer(target: self, action: #selector(tapPageUP))
        let longGesturePageUP = UILongPressGestureRecognizer(target: self, action: #selector(longPageUP))
        let tapGesturePageDOWN = UITapGestureRecognizer(target: self, action: #selector(tapPageDOWN))
        let longGesturePageDOWN = UILongPressGestureRecognizer(target: self, action: #selector(longPageDOWN))
        let tapGestureSortContacts = UITapGestureRecognizer(target: self, action: #selector(tapSortContacts))
        longGestureSortContacts = UILongPressGestureRecognizer(target: self, action: #selector(longSortContacts))
        let tapGestureSearch = UITapGestureRecognizer(target: self, action: #selector(tapSearch))
        let longGestureSearch = UILongPressGestureRecognizer(target: self, action: #selector(longSearch))
        
        BackButton.addGestureRecognizer(tapGestureBack)
        BackButton.addGestureRecognizer(longGestureBack)
        Contact1.addGestureRecognizer(tapGestureC1)
        Contact1.addGestureRecognizer(longGestureC1)
        Contact2.addGestureRecognizer(tapGestureC2)
        Contact2.addGestureRecognizer(longGestureC2)
        Contact3.addGestureRecognizer(tapGestureC3)
        Contact3.addGestureRecognizer(longGestureC3)
        PageUpButton.addGestureRecognizer(tapGesturePageUP)
        PageUpButton.addGestureRecognizer(longGesturePageUP)
        PageDown.addGestureRecognizer(tapGesturePageDOWN)
        PageDown.addGestureRecognizer(longGesturePageDOWN)
        SortContacts.addGestureRecognizer(tapGestureSortContacts)
    SortContacts.addGestureRecognizer(longGestureSortContacts)
        SearchButton.addGestureRecognizer(tapGestureSearch)
        SearchButton.addGestureRecognizer(longGestureSearch)
        
        HelperUI.disableAndMakeUIButtonIvisible(Button: PageUpButton)
        HelperUI.disableAndMakeUIButtonIvisible(Button: Contact1)
        HelperUI.disableAndMakeUIButtonIvisible(Button: Contact2)
        HelperUI.disableAndMakeUIButtonIvisible(Button: Contact3)
        
        contactArray += [Contact1, Contact2, Contact3]
        CurrentUser.FirebaseDocRef?.collection("contacts").getDocuments() { (ContactSnapshot, err) in
            if let err = err {
                print("Error getting contacts in Messages.viewDidLoad: \(err)")
            }
            else {
                if ContactSnapshot?.count == 0{
                    self.OrderLabel.text = "Add a contact"
                    for contact in self.contactArray{
                        HelperUI.disableAndMakeUIButtonIvisible(Button: contact)
                    }
                }
                else{
                    let query = CurrentUser.FirebaseDocRef?.collection("messages").order(by: "LastMessageTime", descending: true).limit(to: 3)
                    query?.getDocuments(){ (QuerySnapshot,err) in
                        if let err = err {
                            print("Error getting contacts in Messages.viewDidLoad: \(err)")
                        }
                        else{
                            var i = 0
                            if let snap = QuerySnapshot{
                                for document in snap.documents{
                                    HelperUI.enableUIButton(Button: self.contactArray[i])
                                    var name = document.data()["uid"] as? String ?? ""
                                    let TrustedQuery = CurrentUser.FirebaseDocRef?.collection("contacts").whereField("uid", isEqualTo: name).limit(to: 1)
                                    TrustedQuery?.getDocuments() { (TrustedSnapshot, err) in
                                        if let err = err {
                                            print("Error getting contacts in Messages.ToggleRecentandAlphabeticalandTrusted: \(err)")
                                        }
                                        else{
                                            HelperUI.enableUIButton(Button: self.contactArray[i])
                                            if let TrustedSnap = TrustedSnapshot {
                                                for TrustedDoc in TrustedSnap.documents{
                                                    if TrustedDoc.data()["trusted"] as? Bool == true {
                                                        name += "⭑"
                                                        self.contactArray[i].setTitle(name, for: .normal)
                                                        i += 1
                                                    }
                                                    else{
                                                        self.contactArray[i].setTitle(name, for: .normal)
                                                        i += 1
                                                    }
                                                    self.RecentContactArray.append(name)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            let remaining = 3-i
                            if remaining == 1{
                                HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                            }
                            if remaining == 2{
                                HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact2)
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    @objc func tapBack(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            performSegue(withIdentifier: "MessagingToHome", sender: self)
        }
    }
    
    @objc func longBack(){
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "MessagingToHome", sender: self)
        }
        else
        {
            
        }
    }
    
    @objc func tapC1(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            TouchContact(sender: Contact1)
        }
    }
    
    @objc func longC1(){
        if(CurrentUser.LongPress && (longGestureC1.state == UILongPressGestureRecognizer.State.began))
        {
            TouchContact(sender: Contact1)
        }
        else
        {
            
        }
    }
    
    @objc func tapC2(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            TouchContact(sender: Contact2)
        }
    }
    
    @objc func longC2(){
        if(CurrentUser.LongPress && (longGestureC2.state == UILongPressGestureRecognizer.State.began))
        {
            TouchContact(sender: Contact2)
        }
        else
        {
            
        }
    }
    
    @objc func tapC3(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            TouchContact(sender: Contact3)
        }
    }
    
    @objc func longC3(){
        if(CurrentUser.LongPress && (longGestureC3.state == UILongPressGestureRecognizer.State.began))
        {
            TouchContact(sender: Contact3)
        }
        else
        {
            
        }
    }
    
    @objc func tapPageUP(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            TouchPageUp()
        }
    }
    
    @objc func longPageUP(){
        if(CurrentUser.LongPress)
        {
            TouchPageUp()
        }
        else
        {
            
        }
    }
    
    @objc func tapPageDOWN(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            TouchPageDown()
        }
    }
    
    @objc func longPageDOWN(){
        if(CurrentUser.LongPress)
        {
            TouchPageDown()
        }
        else
        {
            
        }
    }
    
    @objc func tapSearch(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            performSegue(withIdentifier: "SegueToSearch", sender: self)
        }
    }
    
    @objc func longSearch(){
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "SegueToSearch", sender: self)
        }
        else
        {
            
        }
    }
    
    @objc func tapSortContacts(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            sort()
        }
    }
    
    @objc func longSortContacts(){
        if(CurrentUser.LongPress  && (longGestureSortContacts.state == UILongPressGestureRecognizer.State.began))
        {
            sort()
        }
        else
        {
            
        }
    }
    
    func TouchPageUp() {
        contactArray += [Contact1, Contact2, Contact3]
        if PageNum - 1 == 0{
            // This tap leads to the top page, disable&Invisible PageUpButton and enable SearchButton
            
            HelperUI.enableUIButton(Button: SearchButton)
            HelperUI.disableAndMakeUIButtonIvisible(Button: PageUpButton)
            SearchButton.setTitle("Search", for: .normal)
            SearchButtonEnabled = true
            PageUpButtonEnabled = false
        }
        PageNum -= 1
        if OrderAlphabetical == true {
            for i in 0...2 {
                HelperUI.enableUIButton(Button: contactArray[2 - i])
                contactArray[i].setTitle(AlphaContactArray[3 * PageNum + i], for: .normal)
            }
        }
        else if OrderTrusted ==  true {
            for i in 0...2 {
                HelperUI.enableUIButton(Button: contactArray[2 - i])
                contactArray[i].setTitle(TrustedContactArray[3 * PageNum + i], for: .normal)
            }
        }
        else if OrderRecent ==  true {
            for i in 0...2 {
                HelperUI.enableUIButton(Button: contactArray[2 - i])
                contactArray[i].setTitle(RecentContactArray[3 * PageNum + i], for: .normal)
            }
        }
    }
    
    func TouchPageDown() {
        contactArray += [Contact1, Contact2, Contact3]
        CurrentUser.FirebaseDocRef?.collection("contacts").getDocuments() { (QuerySnapshot, err) in
            if let err = err {
                print("Error getting contacts in Messages.TouchPageDown: \(err)")
            }
            else {
                if let snapshot = QuerySnapshot {
                    if snapshot.count > 3 * (self.PageNum + 1) {
                        self.PageNum += 1
                        if self.SearchButtonEnabled == true {
                            self.SearchButtonEnabled = false
                            HelperUI.disableAndMakeUIButtonIvisible(Button: self.SearchButton)
                            self.PageUpButtonEnabled = true
                            HelperUI.enableUIButton(Button: self.PageUpButton)
                        }
                        self.SearchButton.setTitle("Page Up", for: .normal)
                        let LastNameOnListWithStar = self.Contact3.titleLabel?.text
                        var Index = LastNameOnListWithStar!.index(LastNameOnListWithStar!.endIndex, offsetBy: 0)
                        var LastNameOnList = LastNameOnListWithStar![..<Index]
                        if LastNameOnListWithStar?.firstIndex(of: "⭑") != nil{
                            Index = LastNameOnListWithStar!.index(LastNameOnListWithStar!.endIndex, offsetBy: -1)
                            LastNameOnList = LastNameOnListWithStar![..<Index]
                        }
            
                        if self.OrderAlphabetical == true {
                            let query = CurrentUser.FirebaseDocRef?.collection("contacts").order(by: "uid").start(after: [LastNameOnList]).limit(to: 3)
                            query?.getDocuments() {(OrderAlphaSnapshot, err) in
                                if let err = err{
                                    print("Error getting contacts in Messages.TouchPageDown: \(err)")
                                }
                                else{
                                    var i = 0
                                    if let snap = OrderAlphaSnapshot{
                                        for document in snap.documents{
                                            HelperUI.enableUIButton(Button: self.contactArray[i])
                                            var name = document.data()["uid"] as? String ?? ""
                                            if document.data()["trusted"] as? Bool == true  {
                                                name += "⭑"
                                            }
                                            self.contactArray[i].setTitle(name, for: .normal)
                                            if self.AlphaContactArray.count - 1 < 3 * self.PageNum + i{
                                                self.AlphaContactArray.insert(name, at: 3 * self.PageNum + i)
                                            }
                                            i += 1
                                        }
                                    }
                                    let remaining = 3 - i
                                    if remaining == 1{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                    }
                                    if remaining == 2{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact2)
                                    }
                                }
                            }
                        }
                        if self.OrderTrusted == true {
                            let query = CurrentUser.FirebaseDocRef?.collection("contacts").whereField("trusted", isEqualTo: true).limit(to: (3 * (self.PageNum + 1)))
                            query?.getDocuments() {(OrderTrustedSnapshot, err) in
                                if let err = err {
                                    print("Error getting contacts in Messages.TouchPageDown: \(err)")
                                }
                                else{
                                    var i = 0
                                    var skip = 1
                                    if let snap = OrderTrustedSnapshot{
                                        for document in snap.documents{
                                            if skip < 3 * self.PageNum + 1 {
                                                skip += 1
                                                continue
                                            }
                                            if document.data()["trusted"] as? Bool == true{
                                                HelperUI.enableUIButton(Button: self.contactArray[i])
                                                let name = (document.data()["uid"] as? String ?? "") + "⭑"
                                                self.contactArray[i].setTitle(name, for: .normal)
                                                if self.TrustedContactArray.count - 1 < 3 * self.PageNum + i{
                                                    self.TrustedContactArray.insert(name, at: 3 * self.PageNum + i)
                                                }
                                                i += 1
                                            }
                                            if i == 2 {
                                                break
                                            }
                                        }
                                    }
                                    let remaining = 3 - i
                                    if remaining == 1{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                    }
                                    if remaining == 2{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact2)
                                    }
                                }
                            }
                        }
                        if self.OrderRecent == true{
                            CurrentUser.FirebaseDocRef?.collection("messages").order(by: "LastMessageTime", descending: true).limit(to: 3 * (self.PageNum + 1)).getDocuments() {(OrderRecentSnapshot, err ) in
                                if let err = err {
                                    print("Error getting contacts in Messages.TouchPageDown: \(err)")
                                }
                                else{
                                    var i = 0
                                    var skip = 1
                                    if let snap = OrderRecentSnapshot{
                                        for document in snap.documents{
                                            if skip < 3 * self.PageNum + 1 {
                                                skip += 1
                                                continue
                                            }
                                            HelperUI.enableUIButton(Button: self.contactArray[i])
                                            if document.data()["trusted"] as? Bool == true {
                                                let name = (document.data()["uid"] as? String ?? "") + "⭑"
                                                self.contactArray[i].setTitle(name, for: .normal)
                                                if self.RecentContactArray.count - 1 < 3 * self.PageNum + i{
                                                    self.RecentContactArray.insert((document.data()["uid"] as? String)!, at: 3 * self.PageNum + i)
                                                }
                                            }
                                            else{
                                                let name = document.data()["uid"] as? String ?? ""
                                                self.contactArray[i].setTitle(name, for: .normal)
                                                if self.RecentContactArray.count - 1 < 3 * self.PageNum + i{
                                                    self.RecentContactArray.insert((document.data()["uid"] as? String)!, at: 3 * self.PageNum + i)
                                                }
                                            }
                                            i += 1
                                            if i == 2{
                                                break
                                            }
                                        }
                                    }
                                    let remaining = 3 - i
                                    if remaining == 1{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                    }
                                    if remaining == 2{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact2)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    func sort() {
        contactArray += [Contact1, Contact2, Contact3]
        if OrderAlphabetical == true {
            OrderAlphabetical = false
            OrderRecent = false
            OrderTrusted = true
            OrderLabel.text = "Trusted Contacts"
            SortContacts.setTitle("🕤", for: .normal)
        }
        else if OrderRecent == true{
            OrderAlphabetical = true
            OrderRecent = false
            OrderTrusted = false
            OrderLabel.text = "Alphabetical Contacts"
            SortContacts.setTitle("⭑", for: .normal)
        }
        else{
            OrderAlphabetical = false
            OrderRecent = true
            OrderTrusted = false
            OrderLabel.text = "Recent Contacts"
            SortContacts.setTitle("𝐀", for: .normal)
        }
        CurrentUser.FirebaseDocRef?.collection("contacts").getDocuments() { (QuerySnapshot, err) in
            if let err = err {
                print("Error getting contacts in Messages.ToggleRecentandAlphabeticalandTrusted: \(err)")
            }
            else {
                if let snapshot = QuerySnapshot {
                    if snapshot.count > 0 {
                        self.PageNum = 0
                        if self.OrderAlphabetical == true {
                            let query = CurrentUser.FirebaseDocRef?.collection("contacts").order(by: "uid").limit(to: 3)
                            query?.getDocuments() {(QuerySnapshot, err) in
                                if let err = err{
                                    print("Error getting contacts in Messages.ToggleRecentandAlphabeticalandTrusted: \(err)")
                                }
                                else{
                                    self.AlphaContactArray.removeAll()
                                    var i = 0
                                    if let snap = QuerySnapshot{
                                        for document in snap.documents{
                                            HelperUI.enableUIButton(Button: self.contactArray[i])
                                            var name = document.data()["uid"] as? String ?? ""
                                            if document.data()["trusted"] as? Bool == true  {
                                                name += "⭑"
                                            }
                                            self.contactArray[i].setTitle(name, for: .normal)
                                            i += 1
                                            self.AlphaContactArray.append(name)
                                        }
                                    }
                                    let remaining = 3-i
                                    if remaining == 1{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                    }
                                    if remaining == 2{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact2)
                                    }
                                }
                            }
                        }
                        else if self.OrderTrusted == true{
                            let query = CurrentUser.FirebaseDocRef?.collection("contacts").whereField("trusted", isEqualTo: true).limit(to: 3)
                            query?.getDocuments() { (QuerySnapshot, err) in
                                if let err = err {
                                    print("Error getting contacts in Messages.ToggleRecentandAlphabeticalandTrusted: \(err)")
                                }
                                else{
                                    self.TrustedContactArray.removeAll()
                                    var i = 0
                                    if let snap1 = QuerySnapshot{
                                        for document in snap1.documents{
                                            HelperUI.enableUIButton(Button: self.contactArray[i])
                                            var name = document.data()["uid"] as? String ?? ""
                                            name += "⭑"
                                            self.contactArray[i].setTitle(name, for: .normal)
                                            i += 1
                                            self.TrustedContactArray.append(name)
                                        }
                                    }
                                    let remaining = 3-i
                                    if remaining == 1{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                    }
                                    if remaining == 2{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact2)
                                    }
                                }
                            }
                        }
                        else if self.OrderRecent == true {
                            let query = CurrentUser.FirebaseDocRef?.collection("messages").order(by: "LastMessageTime", descending: true).limit(to: 3)
                            query?.getDocuments(){ (QuerySnapshot,err) in
                                if let err = err {
                                    print("Error getting contacts in Messages.ToggleRecentandAlphabeticalandTrusted: \(err)")
                                }
                                else{
                                    self.RecentContactArray.removeAll()
                                    var i = 0
                                    if let snap2 = QuerySnapshot{
                                        for document in snap2.documents{
                                            HelperUI.enableUIButton(Button: self.contactArray[i])
                                            var name = document.data()["uid"] as? String ?? ""
                                            let TrustedQuery = CurrentUser.FirebaseDocRef?.collection("contacts").whereField("uid", isEqualTo: name).limit(to: 1)
                                            TrustedQuery?.getDocuments() { (TrustedSnapshot, err) in
                                                if let err = err {
                                                    print("Error getting contacts in Messages.ToggleRecentandAlphabeticalandTrusted: \(err)")
                                                }
                                                else{
                                                    if let TrustedSnap = TrustedSnapshot {
                                                        for TrustedDoc in TrustedSnap.documents{
                                                            HelperUI.enableUIButton(Button: self.contactArray[i])
                                                            if TrustedDoc.data()["trusted"] as? Bool == true {
                                                                name += "⭑"
                                                                self.contactArray[i].setTitle(name, for: .normal)
                                                                i += 1
                                                            }
                                                            else{
                                                                self.contactArray[i].setTitle(name, for: .normal)
                                                                i += 1
                                                            }
                                                            self.RecentContactArray.append(name)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    let remaining = 3-i
                                    if remaining == 1{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                    }
                                    if remaining == 2{
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact3)
                                        HelperUI.disableAndMakeUIButtonIvisible(Button: self.Contact2)
                                    }
                                }
                                
                            }
                        }
                    }
                    
                }
            }
        }
    }
    
    func TouchContact(sender: UIButton) {
        if sender.tag == 1 {
            ContactName = Contact1.currentTitle!
        }
        if sender.tag == 2 {
            ContactName = Contact2.currentTitle!
        }
        if sender.tag == 3 {
            ContactName = Contact3.currentTitle!
        }
        performSegue(withIdentifier: "MessagesToMessage", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let Message = segue.destination as? Message {
            Message.ContactName = ContactName
        }
    }
}
