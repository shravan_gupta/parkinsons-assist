/*
 Settings.swift
 Worked on by: Cooper Birks, Malcolm Mckean, Shravan Gupta, Vi Thai
 Changes:
 -Initial Creation
 -Minor UI changes
 -Added ability to log out
 -Added ability to delete account
 -Added long press button functionality
 Bugs: N/A
 Description: Main file for the settings screen
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import UIKit

import FirebaseCore
import FirebaseFirestore

/* View Controller for the settings screen*/
class Settings: UIViewController {
    var db: Firestore!
    
    @IBOutlet weak var Back: UIButton!
    @IBOutlet weak var Notification: UIButton!
    @IBOutlet weak var VacationMode: UIButton!
    @IBOutlet weak var LongPressButton: UIButton!
    @IBOutlet weak var Background: UIImageView!
    @IBOutlet weak var BackgroundBottom: UIImageView!
    @IBOutlet weak var LogOut: UIButton!
    @IBOutlet weak var DeleteAccount: UIButton!
    @IBOutlet weak var NotificationSwitch: UISwitch!
    @IBOutlet weak var VacationModeSwitch: UISwitch!
    @IBOutlet weak var LongPressSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        db = Firestore.firestore()
        
        LogOut.layer.cornerRadius = LogOut.frame.height * 0.5
        DeleteAccount.layer.cornerRadius = DeleteAccount.frame.height * 0.5
        Back.layer.cornerRadius = Back.frame.height * 0.5
        Notification.layer.cornerRadius = Notification.frame.height * 0.5
        VacationMode.layer.cornerRadius = VacationMode.frame.height * 0.5
        LongPressButton.layer.cornerRadius = LongPressButton.frame.height * 0.5
        Background.layer.cornerRadius = Background.frame.height * 0.35
        BackgroundBottom.layer.cornerRadius = BackgroundBottom.frame.height * 0.35
        LongPressSwitch.isEnabled = false
        VacationModeSwitch.isEnabled = false
        NotificationSwitch.isEnabled = false
        
        let tapGestureLogOut = UITapGestureRecognizer(target: self, action: #selector(tapLogOut))
        let longGestureLogOut = UILongPressGestureRecognizer(target: self, action: #selector(longLogOut))
        let tapGestureBack = UITapGestureRecognizer(target: self, action: #selector(tapBack))
        let longGestureBack = UILongPressGestureRecognizer(target: self, action: #selector(longBack))
        let tapGestureLongPress = UITapGestureRecognizer(target: self, action: #selector(tapLongPress))
        let longGestureLongPress = UILongPressGestureRecognizer(target: self, action: #selector(longLongPress))
        let tapGestureDelete = UITapGestureRecognizer(target: self, action: #selector(tapDelete))
        let longGestureDelete = UILongPressGestureRecognizer(target: self, action: #selector(longDelete))
        
        
        LongPressButton.addGestureRecognizer(tapGestureLongPress)
        LongPressButton.addGestureRecognizer(longGestureLongPress)
        Back.addGestureRecognizer(tapGestureBack)
        Back.addGestureRecognizer(longGestureBack)
        LogOut.addGestureRecognizer(tapGestureLogOut)
        LogOut.addGestureRecognizer(longGestureLogOut)
        DeleteAccount.addGestureRecognizer(tapGestureDelete)
        DeleteAccount.addGestureRecognizer(longGestureDelete)
        
        if(CurrentUser.LongPress)
        {
            LongPressSwitch.isOn = true
        }
        else
        {
            LongPressSwitch.isOn = false
        }
    }
    
    @objc func tapDelete(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            deleteAccountF()
        }
    }
    
    @objc func longDelete(){
        if(CurrentUser.LongPress)
        {
            deleteAccountF()
        }
        else
        {
            
        }
    }
    
    @objc func tapLongPress(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            longPressButtonClicked()
        }
    }
    
    @objc func longLongPress(){
        if(CurrentUser.LongPress)
        {
            longPressButtonClicked()
        }
        else
        {
            //do nothing
        }
    }
    
    @objc func tapBack(){
        if(CurrentUser.LongPress)
        {
            //do nothing
            
        }
        else
        {
            performSegue(withIdentifier: "BackToHome", sender: nil)
        }
    }
    
    @objc func longBack(){
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "BackToHome", sender: nil)
        }
        else
        {
            
        }
    }
   
    @objc func longLogOut(){
        if(CurrentUser.LongPress)
        {
            logOutClicked()
        }
        else
        {
            
        }
    }
    
    @objc func tapLogOut(){
        if(CurrentUser.LongPress)
        {
            
        }
        else
        {
            logOutClicked()
        }
    }
    
    func longPressButtonClicked(){
        if(CurrentUser.LongPress)
        {
            CurrentUser.LongPress = false
            LongPressSwitch.isOn = false
        }
        else
        {
            CurrentUser.LongPress = true
            LongPressSwitch.isOn = true
            print("Enabling Long press")
        }
        CurrentUser.FirebaseDocRef!.updateData(["longPress" : CurrentUser.LongPress])
    }
    
    func deleteAccountF() {
        let alert = UIAlertController(title: "Are you sure you want to delete the account?", message: "Enter password to confirm", preferredStyle: .alert)
        
        alert.addTextField()
        alert.textFields![0].placeholder = "Enter password"
        alert.textFields![0].isSecureTextEntry = true
        alert.addAction(UIAlertAction(title: "Yes", style: .destructive, handler: { action in
            let pwd = alert.textFields![0].text
            let pwdCheck = true //self.checkPwd(password: pwd!)
            
            if pwdCheck == true{
            self.deleteUser()
            if CurrentUser.Username != "master" {
                CurrentUser.Username = ""
                CurrentUser.FirebaseDocRef = nil
                CurrentUser.FirebaseDocId = ""
                self.performSegue(withIdentifier: "segueToLogin", sender: self)
            }
            }
            /*
            else{
                alert.addAction(UIAlertAction(title: "Couldn't delete account", style: .default))
            }
             */
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        self.present(alert,animated:true)
    }
    
    func checkPwd(password: String) -> Bool {
        var checker = false
        db.collection("users").whereField("password", isEqualTo: password).getDocuments() { (querySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            }
            if querySnapshot?.count != 0 {
                checker = true
            }
        }
        return checker
    }
    
    /* Deletes the current user account
     Notice: Master account is non-deletable. */
    private func deleteUser() {
        if let FirDocRef = CurrentUser.FirebaseDocRef {
            if CurrentUser.Username == "master" {
                let alert = UIAlertController(title: "Notice", message: "Master account is non-deletable.", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alert, animated: true)
            } else {
                FirDocRef.delete()
            }
        }
    }
    
    /*Log out*/
    func logOutClicked() {
        CurrentUser.Username = ""
        CurrentUser.FirebaseDocRef = nil
        CurrentUser.FirebaseDocId = ""
        CurrentUser.LongPress = false
        performSegue(withIdentifier: "segueToLogin", sender: self)
    }
    /*
    MARK: - Navigation

    In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        Get the new view controller using segue.destination.
        Pass the selected object to the new view controller.
    }
    */

}
