/*
 SignIn.swift
 Worked on by: Matthew Marinets, Vi Thai, Ramesh Khan, Cooper Birks, Malcolm Mckean, Shravan Gupta
 Changes:
 -Initial Creation
 -Added logo, other UI changes
 -Added logic for invalid logins
 -Changed username and password to labels
 -Added test account
 Bugs: N/A
 Description: View Controller for the sign in screen
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import UIKit

import FirebaseCore
import FirebaseFirestore

/*View controller for the sign in page*/
class SignIn: UIViewController {
    
//    /*For now collecting User data in Dict. Need to reimplement when we hook up firebase.*/
//    var userDB = UserDB()
    var db: Firestore!

    @IBOutlet weak var Username: UITextField!
    @IBOutlet weak var Password: UITextField!
    

    @IBOutlet weak var Error: UILabel!
    
    @IBOutlet weak var GreySquare: UIImageView!
    @IBOutlet weak var LogInButton: UIButton!
    @IBOutlet weak var SignUpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        db = Firestore.firestore()
        //Screen overlay rounded corners
        GreySquare.layer.cornerRadius = GreySquare.frame.height * 0.10
        LogInButton.layer.cornerRadius = LogInButton.frame.height * 0.5
        SignUpButton.layer.cornerRadius = SignUpButton.frame.height * 0.5
        
        //Button borders
        //LogInButton.layer.borderWidth = 2
        //LogInButton.layer.borderColor = UIColor.green.cgColor
        SignUpButton.layer.borderWidth = 2
        SignUpButton.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    /*function called after user attempts to login*/
    @IBAction func pressLogin(_ sender: UIButton) {
        if Username.text != nil, Password.text != nil {
            let uid = Username.text!
            let pw = Password.text!
            let sender = self
            if uid != "", pw != "" {
                
                db.collection("users").whereField("uid", isEqualTo: uid).getDocuments() { (querySnapshot, err) in
                    if let err = err {
                        print("Error getting documents: \(err)")
                    } else {
                        if let snapshot = querySnapshot {
                            if snapshot.count == 0 {
                                sender.Error.text = "Invalid username."
                            } else {
                                for document in snapshot.documents {
                                    var db_pwd : String
                                    db_pwd = document.data()["password"] as? String ?? ""
                                    if db_pwd == pw {
                                        CurrentUser.Username = uid
                                        CurrentUser.FirebaseDocRef = self.db.collection("users").document(document.documentID)
                                        CurrentUser.FirebaseDocId = document.documentID
                                        CurrentUser.LongPress = document.data()["longPress"] as? Bool ?? false
                                        sender.performSegue(withIdentifier: "loginSegue", sender: sender)
                                        break;
                                    } else {
                                        sender.Error.text = "Wrong password. Please retry."
                                    }
                                }
                            }
                        } else {
                            sender.Error.text = "Invalid username."
                        }
                    }
                } // </db.collection.whereField.getDocuments>
                
            } else {
                Error.text = "Please fill in the fields."
            }
        } else {
            Error.text = "Please fill in the fields."
        }
    }
    
    /*function called after user clicks sign up*/
    @IBAction func pressSignUp(_ sender: UIButton) {
        performSegue(withIdentifier: "signUpSegue", sender: self)
    }
    
    /*function called before we segue to the sign up screen*/
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let signUpVC = segue.destination as? SignUp {
//            signUpVC.UserDBLocal = self.userDB
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}
    
    

