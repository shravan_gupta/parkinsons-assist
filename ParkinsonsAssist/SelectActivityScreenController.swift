/*
 SelectActivityScreenController.swift
 Worked on by: Vi Thai, Malcolm Mckean, Matthew Marinets, Cooper Birks
 Changes:
 -Initial Creation
 -Firebase Implementation to allow pushing data to database
 -Added long press button functionality
 -Color coded activities
 Bugs: N/A
 Description: View Controller for activity selection
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import UIKit


/*
 View controller for the activity selection screen
*/
class SelectActivityScreenController: UIViewController {

    @IBOutlet weak var ActivityOne: UIButton!
    @IBOutlet weak var ActivityTwo: UIButton!
    @IBOutlet weak var ActivityThree: UIButton!
    @IBOutlet weak var PageUp: UIButton!
    @IBOutlet weak var PageDown: UIButton!
    @IBOutlet weak var Back: UIButton!
    @IBOutlet weak var DatePicker: UIDatePicker!
    @IBOutlet weak var TopBackground: UIImageView!
    @IBOutlet weak var BottomBackground: UIImageView!
    
//    var CalendarEvents: [Activity] = []
    var ActivityObjects: [Activity] = []
    var PageNum = 1
    var ActivityIndex = 3
    var ActivitiesType = ActivityType.Physical
    var db : Firestore!
    let Formatter = DateFormatter()
    var quickFix1 = true //some hacky stuff but it works, dw about it
    var quickFix2 = true //some hacky stuff but it works, dw about it
    
    override func viewDidLoad() {
        HelperUI.disableUIButton(Button: PageUp)
        PageUp.layer.cornerRadius = PageUp.frame.height * 0.5
        PageDown.layer.cornerRadius = PageDown.frame.height * 0.5
        ActivityOne.layer.cornerRadius = ActivityOne.frame.height * 0.5
        ActivityTwo.layer.cornerRadius = ActivityTwo.frame.height * 0.5
        ActivityThree.layer.cornerRadius = ActivityThree.frame.height * 0.5
        Back.layer.cornerRadius = Back.frame.height * 0.5
        TopBackground.layer.cornerRadius = TopBackground.frame.height * 0.35
        BottomBackground.layer.cornerRadius = BottomBackground.frame.height * 0.35
        
        initActivityObjects()
        
        HelperUI.setUIButtonText(Button: ActivityOne, Text: ActivityObjects[0].ActivityName)
        HelperUI.setUIButtonText(Button: ActivityTwo, Text: ActivityObjects[1].ActivityName)
        HelperUI.setUIButtonText(Button: ActivityThree, Text: ActivityObjects[2].ActivityName)
        
        HelperUI.setUIButtonColor(Button: ActivityOne, Color: Activity.ActivityColorDecoder[ActivityObjects[0].ActivityName])
        HelperUI.setUIButtonColor(Button: ActivityTwo, Color: Activity.ActivityColorDecoder[ActivityObjects[1].ActivityName])
        HelperUI.setUIButtonColor(Button: ActivityThree, Color: Activity.ActivityColorDecoder[ActivityObjects[2].ActivityName])
        
        super.viewDidLoad()
        
        db = Firestore.firestore()
        
        let tapGestureActivity1 = UITapGestureRecognizer(target: self, action: #selector(tapActivity1))
        let longGestureActivity1 = UILongPressGestureRecognizer(target: self, action: #selector(longActivity1))
        let tapGestureActivity2 = UITapGestureRecognizer(target: self, action: #selector(tapActivity2))
        let longGestureActivity2 = UILongPressGestureRecognizer(target: self, action: #selector(longActivity2))
        let tapGestureActivity3 = UITapGestureRecognizer(target: self, action: #selector(tapActivity3))
        let longGestureActivity3 = UILongPressGestureRecognizer(target: self, action: #selector(longActivity3))
        let tapGestureBack = UITapGestureRecognizer(target: self, action: #selector(tapBack))
        let longGestureBack = UILongPressGestureRecognizer(target: self, action: #selector(longBack))
        let tapGesturePageUP = UITapGestureRecognizer(target: self, action: #selector(tapPageUP))
        let longGesturePageUP = UILongPressGestureRecognizer(target: self, action: #selector(longPageUP))
        let tapGesturePageDOWN = UITapGestureRecognizer(target: self, action: #selector(tapPageDOWN))
        let longGesturePageDOWN = UILongPressGestureRecognizer(target: self, action: #selector(longPageDOWN))
        
        ActivityOne.addGestureRecognizer(tapGestureActivity1)
        ActivityOne.addGestureRecognizer(longGestureActivity1)
        ActivityTwo.addGestureRecognizer(tapGestureActivity2)
        ActivityTwo.addGestureRecognizer(longGestureActivity2)
        ActivityThree.addGestureRecognizer(tapGestureActivity3)
        ActivityThree.addGestureRecognizer(longGestureActivity3)
        Back.addGestureRecognizer(tapGestureBack)
        Back.addGestureRecognizer(longGestureBack)
        PageUp.addGestureRecognizer(tapGesturePageUP)
        PageUp.addGestureRecognizer(longGesturePageUP)
        PageDown.addGestureRecognizer(tapGesturePageDOWN)
        PageDown.addGestureRecognizer(longGesturePageDOWN)
    }
    
    /*
     Page Up
     */
    
    @objc func tapPageUP(){
        print(ActivityIndex)
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            pageUpClicked()
        }
    }
    
    @objc func longPageUP(){
        print(ActivityIndex)
        if(CurrentUser.LongPress && quickFix2)
        {
            quickFix2 = false
            quickFix1 = true
            pageUpClicked()
        }
        else
        {
            
        }
    }
    
    @objc func tapPageDOWN(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            print("tapPageDown")
            pageDownClicked()
        }
    }
    
    @objc func longPageDOWN(){
        if(CurrentUser.LongPress && quickFix1)
        {
            quickFix1 = false
            quickFix2 = true
            print("longPageDown")
            pageDownClicked()
        }
        else
        {
            
        }
    }
    
    @objc func tapBack(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            performSegue(withIdentifier: "SegueToSAT", sender: nil)
        }
    }
    
    @objc func longBack(){
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "SegueToSAT", sender: nil)
        }
        else
        {
            
        }
    }
    
    @objc func tapActivity1(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            ActivitySelected(ActivityOne)
        }
    }
    
    @objc func longActivity1(){
        if(CurrentUser.LongPress)
        {
            ActivitySelected(ActivityOne)
        }
        else
        {
            
        }
    }
    
    @objc func tapActivity2(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            ActivitySelected(ActivityTwo)
        }
    }
    
    @objc func longActivity2(){
        if(CurrentUser.LongPress)
        {
            ActivitySelected(ActivityTwo)
        }
        else
        {
            
        }
    }
    
    @objc func tapActivity3(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            ActivitySelected(ActivityThree)
        }
    }
    
    @objc func longActivity3(){
        if(CurrentUser.LongPress)
        {
            ActivitySelected(ActivityThree)
        }
        else
        {
            
        }
    }
    
    func pageUpClicked() {
        PageNum -= 1
        print("pageum is ", PageNum)
        if(PageNum == 1)
        {
            HelperUI.disableUIButton(Button: PageUp)
        }
        ActivityIndex = PageNum * 3
        print(ActivityIndex)
        HelperUI.setUIButtonText(Button: ActivityOne, Text: ActivityObjects[ActivityIndex-3].ActivityName)
        HelperUI.setUIButtonText(Button: ActivityTwo, Text: ActivityObjects[ActivityIndex-2].ActivityName)
        HelperUI.setUIButtonText(Button: ActivityThree, Text: ActivityObjects[ActivityIndex-1].ActivityName)
        
        HelperUI.setUIButtonColor(Button: ActivityOne, Color: Activity.ActivityColorDecoder[ActivityObjects[ActivityIndex-3].ActivityName])
        HelperUI.setUIButtonColor(Button: ActivityTwo, Color: Activity.ActivityColorDecoder[ActivityObjects[ActivityIndex-2].ActivityName])
        HelperUI.setUIButtonColor(Button: ActivityThree, Color: Activity.ActivityColorDecoder[ActivityObjects[ActivityIndex-1].ActivityName])
        
        HelperUI.enableUIButton(Button: ActivityOne)
        HelperUI.enableUIButton(Button: ActivityTwo)
        HelperUI.enableUIButton(Button: ActivityThree)
        HelperUI.enableUIButton(Button: PageDown)
    }
    
    /*
     Page Down
     */
   func pageDownClicked() {
        PageNum += 1
        if(ActivityObjects.count - ActivityIndex >= 3){
            HelperUI.setUIButtonText(Button: ActivityOne, Text: ActivityObjects[ActivityIndex].ActivityName)
            HelperUI.setUIButtonColor(Button: ActivityOne, Color: Activity.ActivityColorDecoder[ActivityObjects[ActivityIndex].ActivityName])
            ActivityIndex += 1
            HelperUI.setUIButtonText(Button: ActivityTwo, Text: ActivityObjects[ActivityIndex].ActivityName)
            HelperUI.setUIButtonColor(Button: ActivityTwo, Color: Activity.ActivityColorDecoder[ActivityObjects[ActivityIndex].ActivityName])
            ActivityIndex += 1
            HelperUI.setUIButtonText(Button: ActivityThree, Text: ActivityObjects[ActivityIndex].ActivityName)
            HelperUI.setUIButtonColor(Button: ActivityThree, Color: Activity.ActivityColorDecoder[ActivityObjects[ActivityIndex].ActivityName])
            ActivityIndex += 1
        }
        else if(ActivityObjects.count - ActivityIndex == 2)
        {
            HelperUI.setUIButtonText(Button: ActivityOne, Text: ActivityObjects[ActivityIndex].ActivityName)
            HelperUI.setUIButtonColor(Button: ActivityOne, Color: Activity.ActivityColorDecoder[ActivityObjects[ActivityIndex].ActivityName])
            ActivityIndex += 1
            HelperUI.setUIButtonText(Button: ActivityTwo, Text: ActivityObjects[ActivityIndex].ActivityName)
            HelperUI.setUIButtonColor(Button: ActivityTwo, Color: Activity.ActivityColorDecoder[ActivityObjects[ActivityIndex].ActivityName])
            ActivityIndex += 1
            
            HelperUI.disableAndMakeUIButtonIvisible(Button: ActivityThree)
        }
        else if(ActivityObjects.count - ActivityIndex == 1)
        {
            HelperUI.setUIButtonText(Button: ActivityOne, Text: ActivityObjects[ActivityIndex].ActivityName)
            HelperUI.setUIButtonColor(Button: ActivityOne, Color: Activity.ActivityColorDecoder[ActivityObjects[ActivityIndex].ActivityName])
            ActivityIndex += 1
            
            HelperUI.disableAndMakeUIButtonIvisible(Button: ActivityTwo)
            HelperUI.disableAndMakeUIButtonIvisible(Button: ActivityThree)
        }
        
        if(ActivityObjects.count == ActivityIndex){
            HelperUI.disableUIButton(Button: PageDown)
        }
        HelperUI.enableUIButton(Button: PageUp)
    }
    
    
    
    
    /*
     Create a list of Activity objects that are used for creating button displays.
     */
    func initActivityObjects(){
        let ActivityStrings = ActivitiesType == ActivityType.Social ? Activity.SocialActivityStrings : Activity.PhysicalActivityStrings
        for ActivityString in ActivityStrings {
            let ActivityObject = Activity(activityString: ActivityString)
            ActivityObjects.append(ActivityObject)
        }
//        for ActivityString in Activity.SocialActivityStrings{
//            let ActivityObject = Activity(activityString: ActivityString)
//            ActivityObjects.append(ActivityObject)
//        }
//        for ActivityString in Activity.PhysicalActivityStrings{
//            let ActivityObject = Activity(activityString: ActivityString)
//            ActivityObjects.append(ActivityObject)
//        }
    }
    
    
    /*
     Transition back to the calendar view
     while adding or updating the new selected activity for selected date to Firebase.
     */
    func ActivitySelected(_ sender: UIButton) {
//        let NewActivity = Activity(activityString: sender.titleLabel!.text!, activityDate: DatePicker.date)
//        CalendarEvents.append(NewActivity)
        
        let NewActivityData : [String : Any] = [
            "activity" : sender.titleLabel!.text! ,
            "date" : DatePicker.date
        ]
        let CalendarCollectionPath = "users/" + CurrentUser.FirebaseDocId + "/calendar"
        Formatter.dateFormat = "yyyyMMdd"
        let docId = Formatter.string(from: DatePicker.date)
        let docRef = db.collection(CalendarCollectionPath).document(docId)
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                docRef.updateData(["activity" : NewActivityData["activity"]])
            } else {
                docRef.setData(NewActivityData)
            }
        }

        performSegue(withIdentifier: "activitySelectedSegue", sender: self)
    }
    
    /*
     Pass the updated calendar events list back to the health screen controller.
     */
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let HealthScreenVC = segue.destination as? HealthScreenController {
//            HealthScreenVC.CalendarEvents = CalendarEvents
//        }
//    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
