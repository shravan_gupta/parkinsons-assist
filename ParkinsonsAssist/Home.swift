/*
 Home.swift
 Worked on by: Malcolm Mckean, Vi Thai, Cooper Birks, Matthew Marinets
 Changes:
 -Initial Creation
 -Minor UI Changes
 -Added long press button functionality
 Bugs: N/A
 Description: Main file for the home screen
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import UIKit

/* View controller for the home screen */
class Home: UIViewController {

    @IBOutlet weak var Settings: UIButton!
    @IBOutlet weak var Health: UIButton!
    @IBOutlet weak var Messaging: UIButton!
    var longPress = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Settings.layer.cornerRadius = Settings.frame.height * 0.25
        Health.layer.cornerRadius = Health.frame.height * 0.25
        Messaging.layer.cornerRadius = Messaging.frame.height * 0.25
        
        let tapGestureSettings = UITapGestureRecognizer(target: self, action: #selector (tapSettings))
        let longGestureSettings = UILongPressGestureRecognizer(target: self, action: #selector (longSettings))
        let tapGestureHealth = UITapGestureRecognizer(target: self, action: #selector (tapHealth))
        let longGestureHealth = UILongPressGestureRecognizer(target: self, action: #selector (longHealth))
        let tapGestureMessaging = UITapGestureRecognizer(target: self, action: #selector (tapMessaging))
        let longGestureMessaging = UILongPressGestureRecognizer(target: self, action: #selector (longMessaging))
        Settings.addGestureRecognizer(tapGestureSettings)
        Settings.addGestureRecognizer(longGestureSettings)
        Health.addGestureRecognizer(tapGestureHealth)
        Health.addGestureRecognizer(longGestureHealth)
        Messaging.addGestureRecognizer(tapGestureMessaging)
        Messaging.addGestureRecognizer(longGestureMessaging)
        // Do any additional setup after loading the view.
//        print("Username: " + CurrentUser.Username)
//        print("FIRDocRef: " + "\(CurrentUser.FirebaseDocRef)")
    }
    
    @objc func tapSettings()
    {
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            performSegue(withIdentifier: "HomeToSettings", sender: nil)
        }
    }
    
    @objc func longSettings()
    {
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "HomeToSettings", sender: nil)
        }
        else
        {
            //do nothing
        }
    }
    
    @objc func tapHealth()
    {
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            performSegue(withIdentifier: "HomeToHealth", sender: nil)
        }
    }
    
    @objc func longHealth()
    {
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "HomeToHealth", sender: nil)
        }
        else
        {
            //do nothing
        }
    }
    
    @objc func tapMessaging()
    {
        if(CurrentUser.LongPress)
        {
            //Do nothing
        }
        else
        {
            performSegue(withIdentifier: "HomeToMessaging", sender: nil)
        }
    }
    
    @objc func longMessaging()
    {
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "HomeToMessaging", sender: nil)
        }
        else
        {
            //do nothing
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
