/*
 HealthScreenController.swift
 Worked on by: Matthew Marinets, Vi Thai, Malcolm Mckean
 Changes:
 -Initial Creation
 -Reimplemented
 -Displayed days with events as a different colour
 -Firebase implementation to allow fetching and listening to data from database
 -Added long press button functionality
 -Color coded activities and display illustrating icon on calendar.
 Bugs: N/A
 Description: This is the View Controller for Health Screen View. It generates a Calendar as Collection View and allows the user to add activities and goals onto the calendar.
 Copyright © 2019 T3am SFU. All rights reserved.
 */

import UIKit

class HealthScreenController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var Back: UIButton!
    @IBOutlet weak var SetActivityGoal: UIButton!
    @IBOutlet weak var AddActivity: UIButton!
    @IBOutlet weak var Calendar: UICollectionView!
    @IBOutlet weak var MonthLabel: UILabel!
    @IBOutlet weak var ActivityGoal: UILabel!
    @IBOutlet weak var CalendarCollectionView: UICollectionView!
    
    let Today = Date()
    let Formatter = DateFormatter()
    var db : Firestore!
    
    // state
//    var CalendarEvents: [Activity] = []
    
    /*
     Initialize the whole view.
     */
    override func viewDidLoad() {
        Back.layer.cornerRadius = Back.frame.height * 0.5
        AddActivity.layer.cornerRadius = AddActivity.frame.height * 0.5
        SetActivityGoal.layer.cornerRadius = SetActivityGoal.frame.height * 0.5
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        db = Firestore.firestore()
        formatMonth()
        fetchAndDisplayGoal()
        
        let tapGestureSAG = UITapGestureRecognizer(target: self, action: #selector(tapSAG))
        let longGestureSAG = UILongPressGestureRecognizer(target: self, action: #selector(longSAG))
        let tapGestureBack = UITapGestureRecognizer(target: self, action: #selector(tapBack))
        let longGestureBack = UILongPressGestureRecognizer(target: self, action: #selector(longBack))
        let tapGestureAddActivity = UITapGestureRecognizer(target: self, action: #selector(tapAddActivity))
        let longGestureAddActivity = UILongPressGestureRecognizer(target: self, action: #selector(longAddActivity))
        
        SetActivityGoal.addGestureRecognizer(tapGestureSAG)
        SetActivityGoal.addGestureRecognizer(longGestureSAG)
        Back.addGestureRecognizer(tapGestureBack)
        Back.addGestureRecognizer(longGestureBack)
        AddActivity.addGestureRecognizer(tapGestureAddActivity)
        AddActivity.addGestureRecognizer(longGestureAddActivity)
        
    }
    
    @objc func tapSAG(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            performSegue(withIdentifier: "SegueToSAG", sender: nil)
        }
    }
    
    @objc func longSAG(){
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "SegueToSAG", sender: nil)
        }
        else
        {
            
        }
    }
    
    @objc func tapBack(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            performSegue(withIdentifier: "SegueToHome", sender: nil)
        }
    }
    
    @objc func longBack(){
        if(CurrentUser.LongPress)
        {
            performSegue(withIdentifier: "SegueToHome", sender: nil)
        }
        else
        {
            
        }
    }
    
    @objc func tapAddActivity(){
        if(CurrentUser.LongPress)
        {
            //do nothing
        }
        else
        {
            pressAddActivity()
        }
    }
    
    @objc func longAddActivity(){
        if(CurrentUser.LongPress)
        {
            pressAddActivity()
        }
        else
        {
            
        }
    }
    
    func fetchAndDisplayGoal() {
        let docRef = CurrentUser.FirebaseDocRef!
        docRef.addSnapshotListener() { (document, error) in
            if let dbGoal = document!.data()!["goal"], "\(dbGoal)" != "" {
                self.ActivityGoal.text = "Current Goal: \(dbGoal) days of activity"
            } else {
                self.ActivityGoal.text = "Activity Goal"
            }
        }
    }
    
    /*
     Specify the behaviour of the "Add Activity" button --
     Transition to the "Add Activity Plan" Screen.
     */
    func pressAddActivity() {
        performSegue(withIdentifier: "addActivitySegue", sender: self)
    }
    
    /*
     Transition to the "Add Activity Plan" Screen, passing the current calendarEvents
     list for modification.
     */
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if let SelectTypeVC = segue.destination as? SelectActivityType {
//            SelectTypeVC.CalendarEvents = CalendarEvents
//        }
//    }
    
    /*
     Formatting the Month to be "LLLL yyyy" e.g November 2019
     */
    func formatMonth() {
        Formatter.dateFormat = "LLLL yyyy"
        MonthLabel.text = Formatter.string(from: Today)
    }
    
    /*
     Returns the number of days in certain month of certain year.
     Returns -1 if month input is not a valid month (not 1 to 12)
     */
    func numberOfDays(month: Int, year: Int) -> Int {
        let DaysInMonth = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        if 1 ... 12 ~= month {
            if month != 2 {
                // index = month - 1
                // Because array starts with index 0.
                return DaysInMonth[month - 1]
            } else {
                // Checking for leap year
                if isLeapYear(year: year) {
                    return 29
                } else {
                    return 28
                }
            }
        } else {
            return -1
        }
    }
    
    /*
    Checking if the given year is a leap year.
     */
    func isLeapYear(year: Int) -> Bool {
        if year % 4 == 0 {
            if year % 100 == 0 {
                if year % 400 == 0 {
                    return true
                } else {
                    return false
                }
            } else {
                return true
            }
        } else {
            return false
        }
    }
    
    /*
     Calculating and determining the index of first weekday of current month.
     e.g First weekday of Nov 2019 is a Friday,
     this func will return 5 since 5 is the index of "Fri" in Weekdays array.
     */
    func indexOfFirstWeekdayOfMonth() -> Int {
        let Weekdays = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"]
        Formatter.dateFormat = "EEE"
        let TodayWeekday = Formatter.string(from: Today)
        let TodayWeekdayEnum = Weekdays.index(of: TodayWeekday)!
        Formatter.dateFormat = "dd"
        let TodayDate = Int(Formatter.string(from: Today))!
        var FirstWeekdayOfMonthEnum = TodayWeekdayEnum - (TodayDate % 7 - 1)
        FirstWeekdayOfMonthEnum = FirstWeekdayOfMonthEnum < 0 ? FirstWeekdayOfMonthEnum + 7 : FirstWeekdayOfMonthEnum
        return FirstWeekdayOfMonthEnum
    }
    
    /*
     Returns the number of cells that are in the calendar collectionView object.
     */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        Formatter.dateFormat = "MM"
        let month = Int(Formatter.string(from: Today))!
        Formatter.dateFormat = "yyyy"
        let year = Int(Formatter.string(from: Today))!
        
        let FirstWeekday = indexOfFirstWeekdayOfMonth()
        let NumberOfEmptyCells = FirstWeekday == 0 ? 6 : FirstWeekday - 1
        
        return numberOfDays(month: month, year: year) + NumberOfEmptyCells
    }
    
    /*
     Creates and formats the cell at a certain position of the calendar collectionView object.
     */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let Cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalendarDate", for: indexPath) as! CalendarViewCell
        
        let FirstWeekday = indexOfFirstWeekdayOfMonth()
        let NumberOfEmptyCells = FirstWeekday == 0 ? 6 : FirstWeekday - 1
        
        Cell.DateLabel.text = "\(indexPath.row + 1 - NumberOfEmptyCells)"
        
        // If the cell is Today, red borders it.
        Formatter.dateFormat = "dd"
        if Int(Formatter.string(from: Today)) == Int(Cell.DateLabel!.text!) {
            Cell.layer.borderColor = #colorLiteral(red: 1, green: 0, blue: 0, alpha: 1)
            Cell.layer.borderWidth = 5
        }
        
        // If the cell doesn't represent a valid day of the month, hide it.
        if Int(Cell.DateLabel.text!)! <= 0 {
            Cell.isHidden = true
        }
        
        // If there is an event registered for this day, set the background colour.
//        for event in CalendarEvents {
//            Formatter.dateFormat = "dd"
//            let ActivityDay = Int(Formatter.string(from: event.ActivityDate))!
//            Formatter.dateFormat = "MM-yyyy"
//            let ActivityMonthYear = Formatter.string(from: event.ActivityDate)
//            let TodayMonthYear = Formatter.string(from: self.Today)
//            if (Int(Cell.DateLabel!.text!) == Int(ActivityDay)) && (TodayMonthYear == ActivityMonthYear) {
//                Cell.backgroundColor = #colorLiteral(red: 0.7768511465, green: 0.1675807909, blue: 0.9255481362, alpha: 1)
//            }
//        }
        
        let CalendarCollectionPath = "users/" + CurrentUser.FirebaseDocId + "/calendar"
        var CellLabel = Cell.DateLabel!.text!
        CellLabel = CellLabel.count == 1 ? "0" + CellLabel : CellLabel
        Formatter.dateFormat = "yyyyMM"
        let docId = Formatter.string(from: Today) + CellLabel
        let docRef = db.collection(CalendarCollectionPath).document(docId)

//        docRef.getDocument { (document, error) in
//            if let document = document, document.exists {
//                self.Formatter.dateFormat = "yyyyMMdd"
//                let TodayIntVal = Int(self.Formatter.string(from: self.Today))!
//                let EventIntVal = Int(docId)!
//                // Set background color depending if it's past Event or future Event
//                if EventIntVal < TodayIntVal {
//                    Cell.backgroundColor = #colorLiteral(red: 0.8559886813, green: 0.2249320149, blue: 0.4726090431, alpha: 0.5)
//                } else {
//                    Cell.backgroundColor = #colorLiteral(red: 0.8559886813, green: 0.2249320149, blue: 0.4726090431, alpha: 1)
//                }
//            } else {
//                Cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
//            }
//        }
        
        docRef.addSnapshotListener { (documentSnapshot, error) in
            if let document = documentSnapshot, document.exists {
                let Event = document.data()!["activity"]! as! String
                Cell.backgroundColor = Activity.ActivityColorDecoder[Event]
                Cell.Icon.text = Activity.ActivityIconDecoder[Event]
                
                self.Formatter.dateFormat = "yyyyMMdd"
                let TodayIntVal = Int(self.Formatter.string(from: self.Today))!
                let EventIntVal = Int(docId)!
                // Set background color depending if it's past Event or future Event
                if EventIntVal < TodayIntVal {
//                    Cell.backgroundColor = #colorLiteral(red: 0.8559886813, green: 0.2249320149, blue: 0.4726090431, alpha: 0.5)
                    Cell.alpha = 0.5
                } else {
//                    Cell.backgroundColor = #colorLiteral(red: 0.8559886813, green: 0.2249320149, blue: 0.4726090431, alpha: 1)
                    Cell.alpha = 1
                }
            } else {
                Cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
                Cell.Icon.text = ""
            }
        }
 
        
        return Cell
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
